import { Options, Vue } from 'vue-class-component';
import Header from './components/Header';
// eslint-disable-next-line import/extensions, import/no-named-as-default-member
import Tasks from './components/Tasks';
import AddTask from './components/AddTask.vue';
import { ITask } from './interfaces/ITask';

import './App.css';

@Options({
  components: {
    AddTask,
    Header,
    Tasks,
  },
})
export default class App extends Vue {
  tasks = [] as ITask[];

  showAddTask = false;

  api = {
    tasksUrl: 'http://localhost:5000/tasks',
  }

  async addTask(task: ITask) {
    const res = await fetch(`${this.api.tasksUrl}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(task),
    });
    const data = await res.json();
    this.tasks = [...this.tasks, data];
  }

  async created() {
    const newTasks = await this.fetchTasks();
    console.log({ newTasks });
    this.tasks = newTasks;
  }

  async deleteTask(id: number) {
    const res = await fetch(`${this.api.tasksUrl}/${id}`, {
      method: 'DELETE',
      // headers: { 'Content-Type': 'application/json' },
    });
    if (res.status === 200) {
      this.tasks = this.tasks.filter((t) => t.id !== id);
    }
  }

  toggleAddTask() {
    this.showAddTask = !this.showAddTask;
  }

  // eslint-disable-next-line class-methods-use-this
  async toggleReminder(id: number) {
    const taskToToggle = await this.fetchTask(id);
    const newTask = {
      ...taskToToggle,
      reminder: !taskToToggle.reminder,
    };
    const res = await fetch(`${this.api.tasksUrl}/${id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(newTask),
    });
    const data = await res.json();
    this.tasks = this.tasks.map((t) => (
      (t.id === data.id ? { ...data } : { ...t })
    ));
  }

  async fetchTasks() {
    const res = await fetch(this.api.tasksUrl);
    return res.json();
  }

  // eslint-disable-next-line class-methods-use-this
  async fetchTask(id: number) {
    const res = await fetch(`${this.api.tasksUrl}/${id}`);
    return res.json();
  }

  render() {
    return (
      <div class="container">
        <Header
          title={'Task Tracker'}
          onToggleAddTask={this.toggleAddTask}
          showAddTask={this.showAddTask}
        />
        {
          this.showAddTask
          && <AddTask onAdd-task={this.addTask} />
        }
        <Tasks
          tasks={this.tasks}
          onDeleteTask={this.deleteTask}
          onToggleReminder={this.toggleReminder}
        />
      </div>
    );
  }
}
