import { defineComponent, PropType } from 'vue';
import { identity } from '@jamesgmarks/utilities';
import Task from './Task.vue';
import { ITask } from '../interfaces/ITask';

export default defineComponent({
  name: 'Tasks',
  components: {
    Task,
  },
  props: {
    tasks: {
      type: Array,
      default: () => [] as ITask[],
    },
    onDeleteTask: {
      type: Function as PropType<(taskId: number) => void>,
      default: identity,
    },
    onToggleReminder: {
      type: Function as PropType<(taskId: number) => void>,
      default: identity,
    },
  },
  methods: {},
  render({ tasks }: { tasks: ITask[] }) {
    return (
      <>
        {(tasks ?? []).map((task) => (
          <div key={task.id}>
            <Task
              task={task}
              onDeleteTask={() => {
                if (task.id) this.onDeleteTask(task.id);
              }}
              onToggleReminder={() => {
                if (task.id) this.onToggleReminder(task.id);
              }}
            />
          </div>
        ))}
      </>
    );
  },
});
