import { identity } from '@jamesgmarks/utilities';
import { defineComponent, PropType } from 'vue';
import Button from './Button';
import SubHeading from './SubHeading';

const headerStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginBottom: '20px',
};

const Header = defineComponent({
  name: 'Header',
  components: {
    Button,
    SubHeading,
  },
  props: {
    title: {
      type: String,
      default: 'Hello World',
    },
    showAddTask: {
      type: Boolean,
      default: false,
    },
    onToggleAddTask: {
      type: Function as PropType<() => void>,
      default: identity,
    },
  },
  render() {
    return (
      <>
        <header style={headerStyle}>
          <h1 onClick={() => console.log('title clicked')}>{this.title}</h1>
          <Button
            text={`Add Task ${this.showAddTask ? '-' : '+'}`}
            color={this.showAddTask ? 'blue' : 'green'}
            onClick={() => this.onToggleAddTask()}
          />
        </header>
        <header>
          <SubHeading text={(new Date()).toLocaleDateString()} />
        </header>
      </>
    );
  },
});

export default Header;
