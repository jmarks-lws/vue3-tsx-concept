const SubHeading = ({ text }: { text: string }): JSX.Element => (
  <h4>{text}</h4>
);

export default SubHeading;
