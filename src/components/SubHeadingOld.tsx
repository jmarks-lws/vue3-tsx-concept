import { Options, Vue } from 'vue-class-component';

import Button from './Button.vue';

@Options({
  components: {
    Button,
  },
  props: {
    title: {
      type: String,
      default: 'Hello World',
    },
  },
})
export default class SubHeadingOld extends Vue {
  private test = 'abc';

  render(): JSX.Element {
    if (this.test === 'abc') {
      this.test = 'cba';
    } else {
      this.test = 'abc';
    }
    return (
      <h1>This is a subheading written in JSX</h1>
    );
  }
}
