import { identity } from '@jamesgmarks/utilities';
import { defineComponent, PropType } from 'vue';

const Button = defineComponent({
  props: {
    text: String,
    color: String,
    onClick: {
      type: Function as PropType<CallableFunction>,
      default: identity,
    },
  },
  data: () => ({ }),
  render({ text, color }: { text: string, color: string }): JSX.Element {
    return (
      <header>
        <button
          class="btn"
          style={{ background: color }}
          onClick={() => {
            this.onClick();
          }}
        >{text}</button>
      </header>
    );
  },
});

export default Button;
